﻿using System;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria;

namespace NoHeartsStars {
	public class NoHeartsStars : Mod {
		public NoHeartsStars() {
			Properties = new ModProperties {
				Autoload = true
			};
		}
	}

	public class GItem : GlobalItem {
		public override void SetDefaults(Item item) {
			if (item.type == ItemID.Heart || item.type == ItemID.Star) {
				item.SetDefaults(0, false);
			}
		}
	}
}
